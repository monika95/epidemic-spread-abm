import java.util.ArrayList;
import java.util.Random;
import javax.swing.JFrame;

public class MainSimulation {

    private static SimulationPanel panel;
    public static final int height = 600;
    public static final int width = 600;

    public static ArrayList<Agent> population;
    public static Agent[][] grid;
    public static final int populationSize = 5000;
    public static final int initInfectedPopulation = 10;
    public static final double infectionProbability = 0.7;
    private static final int neighborhoodSize = 7;

    public static void main(String[] args) throws InterruptedException {
        simulate();
    }

    public static void simulate() throws InterruptedException {
        createPeople();
        createInfectionSources();
        visualize();

        boolean end = false;
        while (!end) {
            end = calculateInfection();
            panel.repaint();
            Thread.sleep(30);
        }
    }

    private static boolean calculateInfection() {
        boolean spreadInfection = true;
        for (Agent agent : population) {
            agent.update();
            if (agent.isInfected()) {
                spreadInfection = false;
                infectNeighbors(agent);
            }
        }
        return spreadInfection;
    }

    private static void infectNeighbors(Agent agent) {
        for (int x = agent.getX() - neighborhoodSize / 2; x < agent.getX() + neighborhoodSize / 2; x++) {
            for (int y = agent.getY() - neighborhoodSize / 2; y < agent.getY() + neighborhoodSize / 2; y++) {
                if (x == agent.getX() && y == agent.getY())
                    continue;
                Agent neighbor = x >= 0 && x < width && y >= 0 && y < height ? grid[x][y] : null;

                if (neighbor != null && !neighbor.isResistant() && !neighbor.isInfected() && Math.random() < infectionProbability) {
                    neighbor.setInfected(true);
                }
            }
        }
    }

    private static void createPeople() {
        population = new ArrayList<>();
        grid = new Agent[width][height];

        for (int i = 0; i < populationSize; i++) {
            Agent newAgent = new Agent();
            population.add(newAgent);
            grid[newAgent.getX()][newAgent.getY()] = newAgent;
        }
    }

    private static void createInfectionSources() {
        for(int i = 0; i < initInfectedPopulation; i++){
            Random rand = new Random();
            int num = rand.nextInt(population.size());
            population.get(num).setInfected(true);
        }
    }

    private static void visualize() throws InterruptedException {
        panel = new SimulationPanel();
        JFrame mainWindow = new JFrame(" Flu epidemy simulation");
        mainWindow.setSize(height, width);
        mainWindow.add(panel);
        mainWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainWindow.setVisible(true);

        panel.repaint();
        Thread.sleep(2000);
    }
}