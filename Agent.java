public class Agent {

    private int x, y;
    private double xVelocity, yVelocity;

    private int infectionDuration;
    public static final int agentSize = 4;
    private boolean infected;
    private boolean resistant;

    private final static int infectionTime = 100;
    private final static int step = 1;

    public Agent() {
        boolean collision;
        do {
            this.x = (int) (Math.random() * MainSimulation.width);
            this.y = (int) (Math.random() * MainSimulation.height);

            collision = false;
            for (int i = this.getX() - agentSize / 2; i <= this.getX() + agentSize / 2; i++) {
                if (i == this.getX()) continue;
                if (i >= 0 && i < MainSimulation.width && MainSimulation.grid[i][this.getY()] != null) {
                    collision = true;
                    break;
                }
            }
            if (!collision) {
                for (int j = this.getY() - agentSize / 2; j <= this.getY() + agentSize / 2; j++) {
                    if (j == this.getY()) continue;
                    if (j >= 0 && j < MainSimulation.height && MainSimulation.grid[this.getX()][j] != null) {
                        collision = true;
                        break;
                    }
                }
            }
        } while (collision);
    }

    private boolean isMoveAcceptable(int oldX, int oldY, int newX, int newY) {
        if (newX < 0 || newX >= MainSimulation.width || newY < 0 || newY >= MainSimulation.height) return false;

        for (int i = newX - agentSize / 2; i <= newX + agentSize / 2; i++) {
            if (i == oldX) continue;
            if (i >= 0 && i < MainSimulation.width && MainSimulation.grid[i][newY] != null) return true;
        }
        for (int j = newY - agentSize / 2; j <= newY + agentSize / 2; j++) {
            if (j == oldY) continue;
            if (j >= 0 && j < MainSimulation.height && MainSimulation.grid[newX][j] != null) return true;
        }
        return true;
    }

    private void makeMove() {
        int previousX = this.x;
        int previousY = this.y;

        this.x += (int) xVelocity;
        this.y += (int) yVelocity;

        this.xVelocity = 0.5 * this.xVelocity;
        this.yVelocity = 0.5 * this.yVelocity;

        boolean horizontalMove = Math.random() < 0.5;
        int forwardOrBackward = Math.random() < 0.5 ? 1 : -1;

        if (horizontalMove) {
            this.x += forwardOrBackward * step;
            xVelocity += forwardOrBackward;
        } else {
            this.y += forwardOrBackward * step;
            yVelocity += forwardOrBackward;
        }

        boolean moveValid = isMoveAcceptable(previousX, previousY, this.getX(), this.getY());
        if (!moveValid) {
            this.x = previousX;
            this.y = previousY;
            this.xVelocity = 0;
            this.yVelocity = 0;
        } else {
            MainSimulation.grid[previousX][previousY] = null;
            MainSimulation.grid[this.getX()][this.getY()] = this;
        }
    }

    public void update() {
        updateState();
        makeMove();
    }

    private void updateState() {
        if (this.infected) {
            infectionDuration--;
            if (infectionDuration <= 0) {
                this.infected = false;
                this.resistant = true;
            }
        }
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public boolean isInfected() {
        return this.infected;
    }

    public boolean isResistant() {
        return this.resistant;
    }

    public void setInfected(boolean b) {
        this.infected = b;
        this.infectionDuration = infectionTime;
    }
}