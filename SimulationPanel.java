import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

public class SimulationPanel extends JPanel {

    private static final Color backgroundColor = Color.white;
    private static final Color resistantColor = new Color(96, 204, 255);
    private static final Color infectedColor = new Color(218, 80, 96);
    private static final Color susceptibleColor = new Color(68, 204, 61);

    @Override
    protected void paintComponent(Graphics g) {
        g.setColor(backgroundColor);
        g.fillRect(0, 0, getWidth(), getHeight());

        for (Agent agent : MainSimulation.population) {
            if (agent.isInfected()) {
                g.setColor(infectedColor);
            } else if (agent.isResistant()) {
                g.setColor(resistantColor);
            } else {
                g.setColor(susceptibleColor);
            }

            g.fillRect(agent.getX() - Agent.agentSize / 2, agent.getY() - Agent.agentSize / 2,
                    Agent.agentSize, Agent.agentSize);
        }
    }
}